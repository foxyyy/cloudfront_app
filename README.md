# POC

## Dir structure
- _scripts
- app
    - lambdas
    - spa
- terraform
    - profiles
        - dev
    - modules
        - alerts
        - backend
        - spa

## Build

Use _scripts/package.sh to build app and copy artifacts to infra s3
Use _scripts/deploy.sh to run deploy process
Use _scripts/cloudfront_invalidation.sh to invalidate cdn cache
Use _scripts/destroy.sh to destroy infrastructure
