build_number=$1
env=$2
infra_s3_name=$3
aws_profile=$4

if [ -z "$build_number" ]; then
    echo "Build number is empty. Build number - latest"
    build_number="latest"
fi
if [ -z "$env" ]; then
    echo "env name is empty. Build number - latest"
    env="dev"
fi
if [ -z "$infra_s3_name" ]; then
    echo "Infra s3 bucket is empty. Use default hw43.terraform"
    infra_s3_name="hw43.terraform"
fi
if [ -z "$aws_profile" ]; then
    echo "AWS profile name is empty. Use default"

fi

root_location=$(pwd)
echo "Current location:\n$(pwd)"
echo "Copy s3://$infra_s3_name/packages/app/spa_$build_number/index.html.tpl to tf folder"
aws s3 cp s3://$infra_s3_name/packages/app/spa_$build_number/index.html.tpl $root_location/terraform/profiles/$env $aws_profile

echo "Go to tf dir"
cd $root_location/terraform/profiles/$env
ls
terraform init 
terraform plan -var="build_version=$build_number"
terraform apply -var="build_version=$build_number" -auto-approve


aws_account_id=$(aws sts get-caller-identity --query "Account" --output text $aws_profile)

echo "Copy s3://$infra_s3_name/packages/app/spa_$build_number/* to s3://$env-$aws_account_id-spa"
aws s3 cp s3://$infra_s3_name/packages/app/spa_$build_number/ s3://$env-$aws_account_id-spa \
    --recursive --include '*' --exclude 'index.html.tpl' $aws_profile

echo "Copy $root_location/terraform/profiles/$env/index.html to s3://$env-$aws_account_id-spa"
aws s3 cp $root_location/terraform/profiles/$env/index.html s3://$env-$aws_account_id-spa $aws_profile



