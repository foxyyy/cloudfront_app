def handler(event, context):
    resp = {
        "isBase64Encoded": "true",
        "statusCode": 200,
        "headers":  {
            "Access-Control-Allow-Origin" : "*",
            "Access-Control-Allow-Credentials" : "true"
            },
        "body": "Hello world"
    }
    return resp