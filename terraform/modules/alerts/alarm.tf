resource "aws_cloudwatch_metric_alarm" "lambda_errors" {
  count = length(var.lambda_names)

  alarm_name          = "${var.env}-${var.lambda_names[count.index]}-lambda-errors"
  alarm_description   = "Lambda with errors"
  metric_name         = "Errors"
  namespace           = "AWS/Lambda"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  statistic           = "Maximum"
  threshold           = "0"
  period              = "60"
  evaluation_periods  = "1"
  actions_enabled     = "true"
  alarm_actions       = [aws_sns_topic.alerts.arn]
  #   ok_actions          = [aws_sns_topic.sns.arn]
  dimensions = {
    FunctionName = "${var.env}-${var.lambda_names[count.index]}"
  }

  tags = var.tags
}