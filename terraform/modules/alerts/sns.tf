#role and access to sns
resource "aws_sns_topic" "alerts" {
  name         = "${var.env}-alerts"
  display_name = "${var.env}-alerts"

  delivery_policy = <<EOF
{
  "http": {
    "defaultHealthyRetryPolicy": {
      "minDelayTarget": 20,
      "maxDelayTarget": 20,
      "numRetries": 3,
      "numMaxDelayRetries": 0,
      "numNoDelayRetries": 0,
      "numMinDelayRetries": 0,
      "backoffFunction": "linear"
    },
    "disableSubscriptionOverrides": false,
    "defaultThrottlePolicy": {
      "maxReceivesPerSecond": 1
    }
  }
}
EOF
  tags            = var.tags
}

resource "aws_sns_topic_subscription" "alerts" {
  count     = length(var.sns_subscription_list)
  topic_arn = aws_sns_topic.alerts.arn
  protocol  = "email"
  endpoint  = var.sns_subscription_list[count.index]
}