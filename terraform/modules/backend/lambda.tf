resource "aws_lambda_function" "backend" {
  s3_bucket     = var.source_s3_bucket
  s3_key        = "${var.source_s3_key}/lambdas/backend_${var.build_version}.zip"
  function_name = local.backend_lambda_name
  role          = aws_iam_role.backend.arn
  handler       = "backend.handler"


  runtime      = "python3.8"
  memory_size  = 128
  timeout      = 3
  package_type = "Zip"
  tags         = var.tags
}

resource "aws_lambda_permission" "backend" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.backend.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "arn:aws:execute-api:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${aws_api_gateway_rest_api.backend.id}/*/${aws_api_gateway_method.backend_request_get.http_method}${aws_api_gateway_resource.backend_request.path}"
}