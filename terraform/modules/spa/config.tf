data "template_file" "config" {
  template = file("${path.root}/index.html.tpl")
  vars = {
    api_endpoint = "${var.api_endpoint}"
  }
}

resource "local_file" "config" {
  content  = data.template_file.config.rendered
  filename = "${path.root}/index.html"
}