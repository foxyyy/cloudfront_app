resource "aws_s3_bucket_policy" "public" {
  bucket = aws_s3_bucket.spa.id
  policy = data.aws_iam_policy_document.public.json

}

data "aws_iam_policy_document" "public" {

  statement {
    actions = ["s3:GetObject"]
    resources = [
      aws_s3_bucket.spa.arn,
      "${aws_s3_bucket.spa.arn}/*"
    ]

    principals {
      type = "AWS"
      identifiers = [
        aws_cloudfront_origin_access_identity.spa.iam_arn
      ]
    }
  }
}