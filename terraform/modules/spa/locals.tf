locals {
  spa_bucket_name = "${var.env}-${data.aws_caller_identity.current.account_id}-spa"
}
