variable "env" {}
variable "tags" {}

variable "s3_force_destroy" {}

variable "api_endpoint" {}
variable "acm_cert_id" {}
variable "domain_name" {}