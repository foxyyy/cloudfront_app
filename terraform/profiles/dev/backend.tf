terraform {
  backend "s3" {
    bucket  = "hw43.terraform"
    key     = "tfstate/app/dev.tfstate"
    region  = "eu-central-1"

  }
}