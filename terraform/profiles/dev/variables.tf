variable "env" {
  default = "dev"
}

variable "tags" {
  type = map(any)
  default = {
    env   = "dev"
    owner = "devops-team"
  }
}

variable "source_s3_bucket" {
  default = "hw43.terraform"
}

variable "source_s3_key" {
  default = "packages"
}

variable "build_version" {
  default = "0.0.1"
}

variable "cert" {
  default = "c65d28b2-6abf-4da5-accc-5f445e92a06c"
}

variable "domain" {
  default = "ynastassia.sbs"
}
